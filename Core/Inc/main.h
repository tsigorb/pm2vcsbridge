/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"


/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

#define W6100_Int_Pin GPIO_PIN_1
#define W6100_Int_GPIO_Port GPIOA
#define W6100_RST_Pin GPIO_PIN_2
#define W6100_RST_GPIO_Port GPIOA
#define SPI_Sel_Pin GPIO_PIN_3
#define SPI_Sel_GPIO_Port GPIOA
#define Led_W_Pin GPIO_PIN_12
#define Led_W_GPIO_Port GPIOB
#define DSR3_Pin GPIO_PIN_13
#define DSR3_GPIO_Port GPIOB
#define DTR3_Pin GPIO_PIN_14
#define NRE_DE_1_Pin GPIO_PIN_15
#define NRE_DE_1_GPIO_Port GPIOB
#define RS232_485_Pin GPIO_PIN_6
#define RS232_485_GPIO_Port GPIOC
#define NRE_DE_Pin GPIO_PIN_12
#define NRE_DE_GPIO_Port GPIOC

#define DATA_BUF_SIZE   1400

#pragma pack(1)
typedef struct
{
uint8_t btSIPR[4]; // Src IP addresses
uint16_t wPort; // TcpIp port
uint8_t btMSR[4]; // Subnet mask
uint8_t btGAR[4]; // Gateway address
uint8_t btSHAR[6]; // MAC address - DO NOT USE THIS ON A PUBLIC NETWORK!
} IPPARAMS_TP;

typedef struct
{
  uint16_t wMic;
  uint8_t btVCam,btPreset;
} MICVCAM_TP;

typedef struct
{
  uint8_t Signature[12];
  IPPARAMS_TP Ip;
  int32_t iSpeedRs485;
  MICVCAM_TP MasMicVCam[64];
  uint16_t wProtoIF;
} PM2_VCS_CFG2FLASH_TP;
#pragma pack()

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
