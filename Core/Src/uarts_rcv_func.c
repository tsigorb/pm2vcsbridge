#include "main.h"
#include <string.h>

/////////////////////////////////////////////////////////////////////
extern UART_HandleTypeDef huart1,huart3,huart4;

/////////////////////////////////////////////////////////////////////
// !! huart->RxXferCount=1;  End UART packet (NEED FOR STM UART DRIVER)!! //

void RcvUart1Byte(uint8_t btData)
{ // PC xchg (config)
  uint8_t *pbtBuf=(uint8_t *)(huart1.pRxBuffPtr+huart1.wOff);

  if(huart1.wOff==0) huart1.dwTOut=100;
  if((btData==0x0d) || (btData==0x0a))
  { huart1.RxXferCount=1; }
  else
  {
    if(huart1.RxXferCount==1)
    { // End UART buffer
      btData=0;
    }
  }
  *pbtBuf=btData;
  huart1.wOff++;
}

void RcvUart3Byte(uint8_t btData)
{ // Videocam answer recieve
/*
    *huart->pRxBuffPtr=btData;
    huart->pRxBuffPtr++;
    if((btData==0x0d) || (btData==0x0a))
    {
      huart->RxXferCount=1; // End UART packet (NEED FOR STM UART DRIVER!)
    }
*/
}


void RcvUart4Byte(uint8_t btData)
{ // PM2_VKS_Bridge and PM-2 protocol analizer
  uint8_t *pbtBuf=(uint8_t *)(huart4.pRxBuffPtr+huart4.wOff);

  switch(huart4.btState)
  {
    case 0:
      if(btData==0xff)
      {
        huart4.dwTOut=3;
        huart4.btState=1;
      }
      else
      {
        if(btData==0xfe)
        {
          huart4.dwTOut=3;
          huart4.btState=0x81;
        }
      }
      break;
    case 1:
      if(btData<63)
      { // cmd GetState for PM2_VKS_Bridge or PM2
        if(btData==0)
        { // for PM2_VKS_Bridge
          huart4.RxXferCount=1;
        }
        else huart4.btState=0;
      }
      else
      {
        if(btData==128)
        { // cmd List
          huart4.btState=0x42;
        }
        else
        {
          if((btData>64) && (btData<128))
          { // cmd 1..4
            huart4.btState=0x2;
          }
          else huart4.btState=0;
        }
      }
      break;
    case 2:
      if((btData>0) && (btData<5)) huart4.btState=3;
      else huart4.btState=0;
      break;
    case 3:
      huart4.btState=0; // cmd1..cmd4 for PM2
      break;
    case 0x42: // cmd List byte 3
      if((btData>=4) && (btData<=12))
      {
        huart4.RxXferCount=btData-1;
        huart4.btState=0x43;
      }
      else huart4.btState=0;
      break;
    case 0x43: // cmd List byte 4
      if(btData==5) huart4.btState=0x44;
      else huart4.btState=0;
      break;
    case 0x44: // cmd List byte 5..x
      break;
    case 0x81: // PM2 answer byte 2
      if((btData>64) && (btData<127)) huart4.btState=0x82;
      else huart4.btState=0;
      break;
    case 0x82: // PM2 answer byte 3
      if(btData<63) huart4.btState=0x83;
      else huart4.btState=0;
      break;
    case 0x83: // PM2 answer byte 4
      huart4.btState=0;
      break;
    default:
      huart4.btState=0;
      break;
  }
  if(huart4.btState)
  {
    *pbtBuf=btData;
    huart4.wOff++;
  }
  else
  { // new recieve
    huart4.wOff=0;
    huart4.dwTOut=0;
    huart4.RxXferCount=250;
  }
}
