#include "main.h"
#include <string.h>
#include "wizchip_conf.h"
#include "socket.h"
#include "stdio.h"

/////////////////////////////////////////////////////////////////////
void RcvUart1Byte(uint8_t btData);
void RcvUart3Byte(uint8_t btData);
void RcvUart4Byte(uint8_t btData);

/////////////////////////////////////////////////////////////////////
// g_btTask values
#define d_NeedSendUART1         0x01
//#define d_Need????         0x02
#define d_NeedSendUART4         0x04
#define d_NeedSaveCfg           0x08
#define d_NeedReinitIP          0x10
#define d_VCamNewPosition       0x20
#define d_Mic2VCSState          0x40

#define D_SOCK_WORK             1

#define d_MaxMicActive          8
#define d_MaxMicVCam            64

/////////////////////////////////////////////////////////////////////
SPI_HandleTypeDef hspi1;

UART_HandleTypeDef huart1,huart3,huart4;

IPPARAMS_TP g_IpParams0=
{
{ 192, 168, 2, 100 }, // Src IP address
6060, // Port
{ 255, 255, 255, 0 }, // Subnet mask
{ 0, 0, 0, 0 }, // Gateway address
{ 0x11, 0x22, 0x33, 0xAA, 0xBB, 0x00 }, // MAC address - DO NOT USE THIS ON A PUBLIC NETWORK!
};
/*                    g_IpParamsNew=
{
{ 0, 0, 0, 0 }, // Src IP address
0, // Port
{ 0, 0, 0, 0 }, // Subnet mask
{ 0, 0, 0, 0 }, // Gateway address
{ 0x11, 0x22, 0x33, 0xAA, 0xBB, 0x00 }, // MAC address - DO NOT USE THIS ON A PUBLIC NETWORK!
};*/

MICVCAM_TP MasMicVCam[d_MaxMicVCam];

volatile uint8_t g_btTask;

uint8_t g_btSock=0xff,
        g_btIpEnable=0,
        g_btCmd4PM2,     /* byte 2 = b7b6b5b4b3b2b1b0
                            b7b6=00 - No command
                            b7b6=01 - MicOn PM-2
                            b7b6=10 - MicOff PM-2
                            b7b6=11 - MicOff all PM-2
                            b5b4b3b2b1b0=0..63 � address PM-2 */
        g_btUart1Buf[512],
        g_btUart3Buf[256],
        g_btUart4Buf[256],
        g_btIpBuf[1540];
        
uint8_t MasMicActive[d_MaxMicActive],
        MasMicActiveOld[d_MaxMicActive];

uint8_t g_btCamDef=0,g_btMic=0;

uint16_t g_wProtoIF=0;

int g_iBaudRate3=2400,g_iBaudRate4=115200;

/////////////////////////////////////////////////////////////////////
void Error_Handler(void)
{
  while(1)
  {
  }
}


/* Private function ----------------------------------------------------------*/
static void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  // Initializes the CPU, AHB and APB busses clocks 
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) Error_Handler();

  // Initializes the CPU, AHB and APB busses clocks 
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;
  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) Error_Handler();
}

static void MX_USART1_UART_Init(void)
{
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  if(HAL_UART_Init(&huart1)!=HAL_OK)  Error_Handler();
}

static void MX_USART3_UART_Init()
{
  HAL_GPIO_WritePin(NRE_DE_1_GPIO_Port,NRE_DE_1_Pin,GPIO_PIN_RESET);
  if(g_wProtoIF & 0x03)
  { // RS-485
    HAL_GPIO_WritePin(RS232_485_GPIO_Port,RS232_485_Pin,GPIO_PIN_SET);
  }
  else HAL_GPIO_WritePin(RS232_485_GPIO_Port,RS232_485_Pin,GPIO_PIN_RESET);
  huart3.Instance = USART3;
  huart3.Init.BaudRate = g_iBaudRate3;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  if(HAL_UART_Init(&huart3) != HAL_OK) Error_Handler();
}

static void MX_UART4_Init(void)
{
  huart4.Instance = UART4;
  huart4.Init.BaudRate = g_iBaudRate4;
  huart4.Init.WordLength = UART_WORDLENGTH_8B;
  huart4.Init.StopBits = UART_STOPBITS_1;
  huart4.Init.Parity = UART_PARITY_NONE;
  huart4.Init.Mode = UART_MODE_TX_RX;
  huart4.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart4.Init.OverSampling = UART_OVERSAMPLING_16;
  if(HAL_UART_Init(&huart4) != HAL_OK) Error_Handler();
}

static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();

  /* Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, W6100_RST_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOA, SPI_Sel_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(GPIOB, Led_W_Pin|DTR3_Pin|NRE_DE_1_Pin, GPIO_PIN_SET);
  HAL_GPIO_WritePin(RS232_485_GPIO_Port, RS232_485_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(NRE_DE_GPIO_Port, NRE_DE_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : W6100_Int_Pin */
  GPIO_InitStruct.Pin = W6100_Int_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(W6100_Int_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : W6100_RST_Pin */
  GPIO_InitStruct.Pin = W6100_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(W6100_RST_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : SPI_Sel_Pin */
  GPIO_InitStruct.Pin = SPI_Sel_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(SPI_Sel_GPIO_Port, &GPIO_InitStruct);
  
  /*Configure GPIO pin : Led_W_Pin */
  GPIO_InitStruct.Pin = Led_W_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(Led_W_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : DSR3_Pin */
  GPIO_InitStruct.Pin = DSR3_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(DSR3_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : DTR3_Pin NRE_DE_1_Pin */
  GPIO_InitStruct.Pin = DTR3_Pin|NRE_DE_1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : RS232_485_Pin */
  GPIO_InitStruct.Pin = RS232_485_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLUP;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(RS232_485_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : NRE_DE_Pin */
  GPIO_InitStruct.Pin = NRE_DE_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
  HAL_GPIO_Init(NRE_DE_GPIO_Port, &GPIO_InitStruct);
}

static void MX_SPI1_Init(void)
{
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity =SPI_POLARITY_HIGH; //SPI_POLARITY_LOW; //
  hspi1.Init.CLKPhase = SPI_PHASE_2EDGE;//SPI_PHASE_1EDGE; //
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if(HAL_SPI_Init(&hspi1) != HAL_OK) Error_Handler();
}

int GetBaudRate(int iSpeedInd)
{
  int MasSpeed[4]={1200,2400,4800,9600};
  if(iSpeedInd>3) iSpeedInd=3;
  return MasSpeed[iSpeedInd];
}

/////////////////////////////////////////////////////////////////////
/*void HAL_UART_AbortReceiveCpltCallback(UART_HandleTypeDef *huart)
{
  huart->dwTOut=0;
  huart->btState=0;
  huart->wOff=0;
}*/

void EndRcvPM2Packet(void)
{
  if(huart4.wOff==2)
  { // cmd GetState
    g_btUart4Buf[0]=2; g_btUart4Buf[1]=0xfe; g_btUart4Buf[2]=g_btCmd4PM2;
    g_btTask|=d_NeedSendUART4;
  }
  else
  { // cmd List (0xff,128,4,5,CRC or 0xff,128,5+x,5,...,CRC)
    uint8_t i,btCRC;
    uint16_t wOff;

    btCRC=0;
    wOff=huart4.wOff;
    for(i=1;i<wOff-1;i++) btCRC+=g_btUart4Buf[i];
    if(btCRC==g_btUart4Buf[i])
    {
      if(!(g_btTask & d_Mic2VCSState)) memcpy(MasMicActiveOld,MasMicActive,sizeof(MasMicActive));
      memset(MasMicActive,0,sizeof(MasMicActive));
      if(wOff>5)
      { // list active microphones (1..8)
        for(i=4;(i<wOff-1) && (i<d_MaxMicActive);i++) MasMicActive[i-4]=g_btUart4Buf[i]+1;
      }
      else MasMicActive[0]=0xff; // no active microphones!!!
      g_btUart4Buf[0]=4;
      g_btUart4Buf[1]=0xfe; g_btUart4Buf[2]=64; g_btUart4Buf[3]=0; g_btUart4Buf[4]=0x40;
      g_btTask|=d_NeedSendUART4 | d_VCamNewPosition | d_Mic2VCSState;
    }
  }
}

uint8_t ConvertSimb2Byte(uint8_t btSimb)
{
  if((btSimb>='0') && (btSimb<='9')) return(btSimb-'0');
  if((btSimb>='A') && (btSimb<='F')) return(btSimb-'A'+10);
  if((btSimb>='a') && (btSimb<='f')) return(btSimb-'a'+10);
  return 0;
}

uint8_t ConvertTwoSimb2Byte(uint8_t *pBuf)
{
  return (ConvertSimb2Byte(pBuf[0]) << 4) | ConvertSimb2Byte(pBuf[1]);
}

//               IP     ,Port, Mask  ,  Gate ,Speed
// PM2_VCS+IP:C0A80164,17AC,FFFFFF00,00000000,01C200,0dh

// PM2_VCS+MIC:ProtoIF,MicVCamPreset,...
// PM2_VCS+MIC:00,010101,...,0dh
// ProtoIF: b7..b4 - Protocol, b3..b2 - Speed, b1..b0 - Interface
// MicVCamPreset: b23..b16 - Mic, b15..b8 - VCam, b7..b0 - Preset 
void EndRcvCfgPacket(void)
{ // Recieve config string
  int i,iPos;

  g_btUart1Buf[250]=0;
  if(huart1.wOff>=42)
  {
    if(!memcmp("PM2_VCS+IP:",g_btUart1Buf,11))
    {
      for(i=0;i<4;i++)
      {
        g_IpParams0.btSIPR[i]=ConvertTwoSimb2Byte(g_btUart1Buf+11+(i<<1));
      }
      g_IpParams0.wPort=ConvertTwoSimb2Byte(g_btUart1Buf+20);
      g_IpParams0.wPort=(g_IpParams0.wPort << 8) | ConvertTwoSimb2Byte(g_btUart1Buf+22);
      for(i=0;i<4;i++)
      {
        g_IpParams0.btMSR[i]=ConvertTwoSimb2Byte(g_btUart1Buf+25+(i<<1));
      }
      for(i=0;i<4;i++)
      {
        g_IpParams0.btGAR[i]=ConvertTwoSimb2Byte(g_btUart1Buf+34+(i<<1));
      }
      g_iBaudRate4=ConvertTwoSimb2Byte(g_btUart1Buf+43);
      g_iBaudRate4=(g_iBaudRate4 << 8) | ConvertTwoSimb2Byte(g_btUart1Buf+45);
      g_iBaudRate4=(g_iBaudRate4 << 8) | ConvertTwoSimb2Byte(g_btUart1Buf+47);
      strcpy((char *)g_btUart1Buf,"OK!  \r\n");
      g_btTask|=d_NeedSendUART1;
      return;
    }
  }
  if(huart1.wOff>12)
  {
    if(!memcmp("PM2_VCS+MIC:",g_btUart1Buf,12))
    {
      memset(MasMicVCam,0,sizeof(MasMicVCam));
      g_btCamDef=ConvertTwoSimb2Byte(g_btUart1Buf+12) & 0x0f;
      g_wProtoIF=g_btCamDef;
      g_wProtoIF=(g_wProtoIF << 8) | ConvertTwoSimb2Byte(g_btUart1Buf+14);
      g_iBaudRate3=GetBaudRate((g_wProtoIF >> 2) & 0x03);
      if(huart1.wOff>23)
      {
        i=0; iPos=17;
        while((g_btUart1Buf[iPos]>='0') && (i<d_MaxMicVCam))
        {
          MasMicVCam[i].wMic=ConvertTwoSimb2Byte(g_btUart1Buf+iPos);
          MasMicVCam[i].btVCam=ConvertTwoSimb2Byte(g_btUart1Buf+iPos+2);
          MasMicVCam[i].btPreset=ConvertTwoSimb2Byte(g_btUart1Buf+iPos+4);
          if(g_btUart1Buf[iPos+6]!=',') break;
          iPos+=7; i++;
        }
      }
      strcpy((char *)g_btUart1Buf,"OK!  \r\n");
      g_btTask|=d_NeedSaveCfg | d_NeedSendUART1 | d_NeedReinitIP;
      return;
    }
  }
  strcpy((char *)g_btUart1Buf,"ERROR\r\n");
  g_btTask|=d_NeedSendUART1;
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
  huart->dwTOut=0;
  if(huart==&huart4) EndRcvPM2Packet();
  else
  {
    if(huart==&huart1) EndRcvCfgPacket();
    else ;// ... !!!
  }
}

void RcvUartByte(UART_HandleTypeDef *huart,uint8_t btData)
{ // !!! Analize rcv data for Uart's !!!
  if(huart==&huart4) RcvUart4Byte(btData);
  else
  {
    if(huart==&huart1) RcvUart1Byte(btData);
    else RcvUart3Byte(btData);
  }
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{
  if(huart==&huart4)
  { // switch RS-485 interface to RX mode
    HAL_GPIO_WritePin(NRE_DE_GPIO_Port,NRE_DE_Pin,GPIO_PIN_RESET);
  }
  else
  {
    if(huart==&huart3)
    { // VCam IF
      HAL_GPIO_WritePin(NRE_DE_1_GPIO_Port,NRE_DE_1_Pin,GPIO_PIN_RESET);
      if(g_wProtoIF & 0x03)
      { // RS-485
        HAL_GPIO_WritePin(RS232_485_GPIO_Port,RS232_485_Pin,GPIO_PIN_SET);
      }
      else HAL_GPIO_WritePin(RS232_485_GPIO_Port,RS232_485_Pin,GPIO_PIN_RESET);
    }
  }
}

/////////////////////////////////////////////////////////////////////
uint8_t FindMic(uint8_t btMic,uint8_t *btMasMic)
{
  int i;

  for(i=0;i<d_MaxMicActive;i++)
  {
    if(btMic==btMasMic[i]) return 1;
  }
  return 0;
}

int32_t SendPckt2IpIfNeed(void)
{
  int32_t i,ret;
  uint8_t btMic;

  if(g_btTask & d_Mic2VCSState)
  {
    g_btTask&=~d_Mic2VCSState;
    for(i=0;i<d_MaxMicActive;i++)
    {
      if((MasMicActive[i]!=0) && (MasMicActive[i]!=0xff)) break;
    }
    if(i==d_MaxMicActive)
    {
      ret=send(g_btSock,(uint8_t *)"mic_all_off\r\n",13);
      if(ret<0)
      { g_btTask|=d_NeedReinitIP; return ret; }
    }
    else
    {
      for(i=0;i<d_MaxMicActive;i++)
      {
        btMic=MasMicActive[i];
        if(btMic==0xff) break; // no active microphones
        if(btMic && (!FindMic(btMic,MasMicActiveOld)))
        { // mic_on<SP><NMic><SP>Seat<SP><NSeat><CR><LF>
          sprintf((char *)g_btIpBuf,"mic_on %d Seat %d\r\n",btMic,btMic);
          ret=send(g_btSock,g_btIpBuf,strlen((char *)g_btIpBuf));
          if(ret<0)
          { g_btTask|=d_NeedReinitIP; return ret; }
        }
      }
      for(i=0;i<d_MaxMicActive;i++)
      {
        btMic=MasMicActiveOld[i];
        if(btMic==0xff) break;
        if(btMic && (!FindMic(btMic,MasMicActive)))
        { // mic_off<SP><NMic><CR><LF>
          sprintf((char *)g_btIpBuf,"mic_off %d\r\n",btMic);
          ret=send(g_btSock,g_btIpBuf,strlen((char *)g_btIpBuf));
          if(ret<0)
          { g_btTask|=d_NeedReinitIP; return ret; }
        }
      }
    }
  }
  return SOCK_OK;
}

int32_t TcpIpRcv(void)
{
  int32_t ret;
  datasize_t received_size;

  getsockopt(g_btSock,SO_RECVBUF,&received_size);
  if(received_size>0)
  {
    if(received_size>DATA_BUF_SIZE) received_size=DATA_BUF_SIZE;
    ret=recv(g_btSock,g_btIpBuf,received_size);
    if(ret<=0) return ret;      // check SOCKERR_BUSY & SOCKERR_XXX. For showing the occurrence of SOCKERR_BUSY.
    g_btIpBuf[ret]=0;
    if(!strcmp((char *)g_btIpBuf,"help mic_status\r\n"))
    {
      ret=send(g_btSock,(uint8_t *)"mic_status\r\n",12);
      if(ret<0)
      {
        g_btTask|=d_NeedReinitIP;
        return ret;
      }
    }
  }
  return SOCK_OK;
}

void IpConnection(void)
{
  uint8_t bt_Status,inter;

  if(g_btSock==0xff) return;
  if(g_btIpEnable==0) return;
  g_btIpEnable=0;
  getsockopt(g_btSock, SO_STATUS, &bt_Status);
  switch(bt_Status)
  {
    case SOCK_ESTABLISHED:
      ctlsocket(g_btSock,CS_GET_INTERRUPT,&inter);
      if(inter & Sn_IR_CON)
      {
        inter=Sn_IR_CON;
        ctlsocket(g_btSock,CS_CLR_INTERRUPT,&inter);
      }
      SendPckt2IpIfNeed();
      TcpIpRcv(); 
      return;
    case SOCK_CLOSE_WAIT:
      TcpIpRcv();
      g_btTask|=d_NeedReinitIP;
      break;
    case SOCK_INIT:
      break;
    case SOCK_CLOSED:
      g_btTask|=d_NeedReinitIP;
      break;
    default:
      break;
  }
  return;
}

void ReInitIp(void)
{
  if(g_btSock!=0xff) 
  {
    if(disconnect(g_btSock)==SOCK_BUSY) close(g_btSock);
    g_btSock=0xff;
  }
  g_btTask&=~d_Mic2VCSState;
//  CHIPUNLOCK();
  NETUNLOCK();
  setSIPR(g_IpParams0.btSIPR);
  setSUBR(g_IpParams0.btMSR);
  setGAR(g_IpParams0.btGAR);
  g_IpParams0.btSHAR[5]=g_IpParams0.btSIPR[3];
  setSHAR(g_IpParams0.btSHAR);
  NETLOCK();
//  CHIPLOCK();

/*getSIPR(g_IpParamsNew.btSIPR);
getSUBR(g_IpParamsNew.btMSR);
getGAR(g_IpParamsNew.btGAR);
getSHAR(g_IpParamsNew.btSHAR);*/

 /*{
   static uint8_t btPHYSR;
   btPHYSR=getPHYSR();
   g_IpParamsNew.btSHAR[0]=btPHYSR;
 }*/

  if(socket(D_SOCK_WORK,Sn_MR_TCP4,g_IpParams0.wPort,SF_IO_NONBLOCK)==D_SOCK_WORK)
  {
    if(listen(D_SOCK_WORK)!=SOCK_OK) close(D_SOCK_WORK);
    else
    {
      // need for send all active mic's after reinit IP
      memset(MasMicActiveOld,0,sizeof(MasMicActive));
      g_btSock=D_SOCK_WORK;
      g_btTask&=~d_NeedReinitIP;
      g_btTask|=d_Mic2VCSState;
    }
  }
}

/////////////////////////////////////////////////////////////////////
// 0x0807f800..0x0807ffff - cfg area
extern void FLASH_PageErase(uint32_t PageAddress);

#define d_CfgAddr2Flash         (uint32_t)0x0807f800l

const char *strSignatureCfg="PM2VCSCFG00:";

HAL_StatusTypeDef SaveCfg2Flash(void)
{ // !! WRITE INFO ORDER NO CHAGE (defined by PM2_VCS_CFG2FLASH_TP) !!
  int i;
  uint16_t *pwAddr,*pwData;
  HAL_StatusTypeDef status;

  if(HAL_FLASH_Unlock()!=HAL_OK) return HAL_ERROR;
  FLASH_PageErase(d_CfgAddr2Flash);
  status=FLASH_WaitForLastOperation((uint32_t)FLASH_TIMEOUT_VALUE);
  // After erase operation is completed, disable the PER Bit
  CLEAR_BIT(FLASH->CR,FLASH_CR_PER);
  if(status==HAL_OK)
  {
    pwAddr=(uint16_t *)d_CfgAddr2Flash;
    pwData=(uint16_t *)strSignatureCfg;
    for(i=0;i<6;i++)
    {
      HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,(uint32_t)pwAddr,*pwData);
      pwAddr++; pwData++;
    }
    pwData=(uint16_t *)&g_IpParams0;
    for(i=0;i<sizeof(g_IpParams0)/2;i++)
    {
      HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,(uint32_t)pwAddr,*pwData);
      pwAddr++; pwData++;
    }
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,(uint32_t)pwAddr,g_iBaudRate4 & 0xffff);
    pwAddr++;
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,(uint32_t)pwAddr,g_iBaudRate4 >> 16);
    pwAddr++;
    pwData=(uint16_t *)MasMicVCam;
    for(i=0;i<sizeof(MasMicVCam)/2;i++)
    {
      HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,(uint32_t)pwAddr,*pwData);
      pwAddr++; pwData++;
    }
    HAL_FLASH_Program(FLASH_TYPEPROGRAM_HALFWORD,(uint32_t)pwAddr,g_wProtoIF);
  }
  HAL_FLASH_Lock();
  return status;
}

uint16_t ReadCfgFromFlash(void)
{
  PM2_VCS_CFG2FLASH_TP *pCfg=(PM2_VCS_CFG2FLASH_TP *)0x0807f800;

  if(memcmp(pCfg->Signature,strSignatureCfg,12)) return 0;
  memcpy(&g_IpParams0,&(pCfg->Ip),sizeof(g_IpParams0));
  g_iBaudRate4=pCfg->iSpeedRs485;
  memcpy(MasMicVCam,&(pCfg->MasMicVCam),sizeof(MasMicVCam));
  g_wProtoIF=pCfg->wProtoIF;
  g_iBaudRate3=GetBaudRate((g_wProtoIF >> 2) & 0x03);
  g_btCamDef=(g_wProtoIF >> 8) & 0x0f;
  return 1;
}

/////////////////////////////////////////////////////////////////////
void VCamGotoPreset(uint8_t btVCam,uint8_t btPreset)
{
  if(g_wProtoIF & 0xf0)
  { // Visca command RECALL PRESET - 8x 01 04 3F 02 pp FF - pp: Memory Number preset
    g_btUart3Buf[0]=7;
    g_btUart3Buf[1]=0x80 | (btVCam & 0x07);
    g_btUart3Buf[2]=0x01;
    g_btUart3Buf[3]=0x04;
    g_btUart3Buf[4]=0x3f;
    g_btUart3Buf[5]=0x02;
    g_btUart3Buf[6]=btPreset & 0x7f;
    g_btUart3Buf[7]=0xff;
  }
  else
  { // PELCO-D command SELECT PRESET - 0FFh,Addr,0,7,0,Preset,CS
    g_btUart3Buf[0]=7;
    g_btUart3Buf[1]=0xff;
    g_btUart3Buf[2]=btVCam;
    g_btUart3Buf[3]=0x00;
    g_btUart3Buf[4]=0x07;
    g_btUart3Buf[5]=0x00;
    g_btUart3Buf[6]=btPreset;
    g_btUart3Buf[7]=0x07+g_btUart3Buf[2]+g_btUart3Buf[6];
  }
  HAL_GPIO_WritePin(NRE_DE_1_GPIO_Port,NRE_DE_1_Pin,GPIO_PIN_SET);
  HAL_UART_Transmit_IT(&huart3,g_btUart3Buf+1,g_btUart3Buf[0]);
}

// Control ONLY ONE VCam for FIRST active microphone!
void VCamNewPosition(void)
{
  int i,j;
  uint8_t btMic;

  if(huart3.gState!=HAL_UART_STATE_READY) return;
  g_btTask&=~d_VCamNewPosition;
  for(i=0;i<d_MaxMicActive;i++)
  {
    btMic=MasMicActive[i];
    if(btMic && (btMic!=MasMicActiveOld[i]))
    {
      if(btMic==0xff)
      { // no active microphones -> set ALWAYS default camera position (preset 0)!!!
        if(g_btCamDef && g_btMic) VCamGotoPreset(g_btCamDef,0);
        g_btMic=0;
        return;
      }
      for(j=0;j<d_MaxMicVCam;j++)
      {
        if(btMic==MasMicVCam[j].wMic)
        { // find vcam preset for active microphone
          if(g_btMic==btMic) return;
          g_btMic=btMic;
          VCamGotoPreset(MasMicVCam[j].btVCam,MasMicVCam[j].btPreset);
          return;
        }
      }
    }
  }
}

/////////////////////////////////////////////////////////////////////
int main(void)
{
  HAL_Init();
  SystemClock_Config();
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();

  HAL_Delay(100);
  HAL_GPIO_WritePin(W6100_RST_GPIO_Port, W6100_RST_Pin, GPIO_PIN_SET);
  HAL_Delay(100);

  if(getCIDR()!=0x6100) Error_Handler();
  
  memset(MasMicActive,0,sizeof(MasMicActive));
  memset(MasMicActiveOld,0,sizeof(MasMicActive));
  memset(MasMicVCam,0,sizeof(MasMicVCam));

  if(!ReadCfgFromFlash())
  { SaveCfg2Flash(); }
  MX_USART3_UART_Init();
  MX_UART4_Init();

//VCamGotoPreset(1,0);

  g_btTask=d_NeedReinitIP;
  g_btCmd4PM2=0;
  while(1)
  {
    if(g_btTask & d_NeedSendUART1)
    {
      HAL_UART_Transmit_IT(&huart1,g_btUart1Buf,strlen((char *)g_btUart1Buf));
      g_btTask&=~d_NeedSendUART1;
    }
    if((huart1.gState==HAL_UART_STATE_READY) && (huart1.RxState==HAL_UART_STATE_READY))
    { HAL_UART_Receive_IT(&huart1,g_btUart1Buf,500); }

    /*if((huart3.gState==HAL_UART_STATE_READY) && (huart3.RxState==HAL_UART_STATE_READY))
    { HAL_UART_Receive_IT(&huart3,g_btUart3Buf,4); }*/

    if(g_btTask & d_NeedSendUART4)
    {
      HAL_GPIO_WritePin(NRE_DE_GPIO_Port, NRE_DE_Pin, GPIO_PIN_SET);
      HAL_UART_Transmit_IT(&huart4,g_btUart4Buf+1,g_btUart4Buf[0]);
      g_btTask&=~d_NeedSendUART4;
    }
    if((huart4.gState==HAL_UART_STATE_READY) && (huart4.RxState==HAL_UART_STATE_READY))
    { HAL_UART_Receive_IT(&huart4,g_btUart4Buf,250); }
    
    if(g_btTask & d_NeedSaveCfg)
    {
      if(SaveCfg2Flash()==HAL_OK)
      {
        g_btTask&=~d_NeedSaveCfg;
        if(g_iBaudRate3!=huart3.Init.BaudRate)
        {
          HAL_UART_DeInit(&huart3);
          MX_USART3_UART_Init();
        }
        if(g_iBaudRate4!=huart4.Init.BaudRate)
        {
          HAL_UART_DeInit(&huart4);
          MX_UART4_Init();
        }
      }
    }
    if(g_btTask & d_VCamNewPosition) VCamNewPosition();
    if(g_btTask & d_NeedReinitIP) ReInitIp();
    IpConnection();
  }
}
