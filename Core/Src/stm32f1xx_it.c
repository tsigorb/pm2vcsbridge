#include "main.h"
#include "stm32f1xx_it.h"

/* External variables --------------------------------------------------------*/
extern SPI_HandleTypeDef hspi1;
extern UART_HandleTypeDef huart1,huart3,huart4;

/******************************************************************************/
/*           Cortex-M3 Processor Interruption and Exception Handlers          */ 
/******************************************************************************/
/**
  * @brief This function handles Non maskable interrupt.
  */
void __irq NMI_Handler(void)
{
  /* USER CODE BEGIN NonMaskableInt_IRQn 0 */

  /* USER CODE END NonMaskableInt_IRQn 0 */
  /* USER CODE BEGIN NonMaskableInt_IRQn 1 */

  /* USER CODE END NonMaskableInt_IRQn 1 */
}

/**
  * @brief This function handles Hard fault interrupt.
  */
void __irq HardFault_Handler(void)
{
  /* USER CODE BEGIN HardFault_IRQn 0 */

  /* USER CODE END HardFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_HardFault_IRQn 0 */
    /* USER CODE END W1_HardFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Memory management fault.
  */
void __irq MemManage_Handler(void)
{
  /* USER CODE BEGIN MemoryManagement_IRQn 0 */

  /* USER CODE END MemoryManagement_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_MemoryManagement_IRQn 0 */
    /* USER CODE END W1_MemoryManagement_IRQn 0 */
  }
}

/**
  * @brief This function handles Prefetch fault, memory access fault.
  */
void __irq BusFault_Handler(void)
{
  /* USER CODE BEGIN BusFault_IRQn 0 */

  /* USER CODE END BusFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_BusFault_IRQn 0 */
    /* USER CODE END W1_BusFault_IRQn 0 */
  }
}

/**
  * @brief This function handles Undefined instruction or illegal state.
  */
void __irq UsageFault_Handler(void)
{
  /* USER CODE BEGIN UsageFault_IRQn 0 */

  /* USER CODE END UsageFault_IRQn 0 */
  while (1)
  {
    /* USER CODE BEGIN W1_UsageFault_IRQn 0 */
    /* USER CODE END W1_UsageFault_IRQn 0 */
  }
}

/**
  * @brief This function handles System service call via SWI instruction.
  */
void __irq SVC_Handler(void)
{
  /* USER CODE BEGIN SVCall_IRQn 0 */

  /* USER CODE END SVCall_IRQn 0 */
  /* USER CODE BEGIN SVCall_IRQn 1 */

  /* USER CODE END SVCall_IRQn 1 */
}

/**
  * @brief This function handles Debug monitor.
  */
void __irq DebugMon_Handler(void)
{
  /* USER CODE BEGIN DebugMonitor_IRQn 0 */

  /* USER CODE END DebugMonitor_IRQn 0 */
  /* USER CODE BEGIN DebugMonitor_IRQn 1 */

  /* USER CODE END DebugMonitor_IRQn 1 */
}

void __irq PendSV_Handler(void)
{
}

extern unsigned char g_btIpEnable;
static unsigned short s_wCntr=0,s_wCntrIp=0;

void __irq SysTick_Handler(void)
{
  HAL_IncTick();
  s_wCntr++; s_wCntrIp++;
  if(s_wCntr==500)
  {
    s_wCntr=0;
    HAL_GPIO_TogglePin(Led_W_GPIO_Port, Led_W_Pin);
  }
  if(s_wCntrIp==100)
  {
    s_wCntrIp=0;
    g_btIpEnable=1;
  }
  if(huart1.dwTOut)
  {
    huart1.dwTOut--;
    if(!huart1.dwTOut)
    {
      huart1.btState=0;
      huart1.wOff=0;
      huart1.RxXferCount=250;
      //HAL_UART_AbortReceive_IT(&huart1);
    }
  }
  if(huart3.dwTOut)
  {
    huart3.dwTOut--;
    if(!huart3.dwTOut)
    {
      huart3.btState=0;
      huart3.wOff=0;
      huart3.RxXferCount=500;
    }
  }
  if(huart4.dwTOut)
  {
    huart4.dwTOut--;
    if(!huart4.dwTOut)
    {
      huart4.btState=0;
      huart4.wOff=0;
      huart4.RxXferCount=250;
    }
  }
}

/******************************************************************************/
/* STM32F1xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f1xx.s).                    */
/******************************************************************************/
void __irq SPI1_IRQHandler(void)
{
  HAL_SPI_IRQHandler(&hspi1);
}

void __irq USART1_IRQHandler(void)
{
  HAL_UART_IRQHandler(&huart1);
}

void __irq USART3_IRQHandler(void)
{
  HAL_UART_IRQHandler(&huart3);
}

void __irq UART4_IRQHandler(void)
{
  HAL_UART_IRQHandler(&huart4);
}
